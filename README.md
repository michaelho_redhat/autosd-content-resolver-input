# Autosd Content Resolver Input

This repository contains all the AutoSD configurations for Content Resolver.

See [config specs](https://github.com/minimization/content-resolver/tree/master/config_specs) for reference.
